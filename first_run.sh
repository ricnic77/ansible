#Update
sudo apt update && sudo apt upgrade -y

#Git/curl install
sudo apt install git curl -y
git config --global user.name "Richard Nichols"
git config --global user.email "rick.scsc@outlook.com"

#Ansible setup
sudo apt install ansible -y

#Get Ansible (read only key)
curl http://home.ricknichols.xyz/pub/ansible > ~/.ssh/ansible
